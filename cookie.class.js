/**
 * Cookie Class
 * 
 * BUG, using the method update, the expiration date will always be updated;
 * Example: Cookie will set to expires in 1 day, if tomorrow the cookie will 
 * be updated the expiration date will be add one more day.
 *
 * @author Samuel Ramon
 */
function Cookie() {
	this.days = 1; // default expiration time
	this.value = "";
	this.prefix = "custom_";

    /*
     * Setting up the Cookie parameters like a
     * Constructor
     */
	switch(arguments.length) {
    case 4: this.prefix = arguments[3];
	case 3: this.days= arguments[2];
	case 2: this.value = arguments[1];
	case 1: this.name = arguments[0];
	}

    // Search for a cookie value
	this.searchCookie = function(param) {
        // searching the cookie using Regular Expression
		var re = new RegExp('[; ]'+param+'=([^*;]*)'),
			sMatch = (' '+document.cookie).match(re),
			result = null; // if no cookie or value found return null and set to this.value

        // Checking the cookie value
		if (param && sMatch) {
			result = sMatch[1];
			// Problem with cookies created from JAVA. 
            // JAVA is setting double quotes for the cookie's value
			if (result.indexOf('"') == 0) { // In case of JAVA remove the double quotes
				result = unescape(result.substring(1, result.length-1));
			}
		}
		return result;
	};
}

/**
 * Method to create the cookie. If no arguments are passed, 
 * It'll use the class Constructor default values.
 *
 * @params [name[,value[,days[,prefix]]]]
 */
Cookie.prototype.create = function () {
    // Overwriting class attributes setted by Constructor. 
    // Better used if the default Constructor was used to create the Cookie
	switch(arguments.length) {
    case 4: this.prefix = arguments[3];
	case 3: this.days= arguments[2];
	case 2: this.value = arguments[1];
	case 1: this.name = arguments[0];
	}
	var d = new Date();
	d.setTime(d.getTime()+(this.days*24*60*60*1000));
	var expires = d.toGMTString(); // cookies works with GMT timezone
	document.cookie = this.prefix+this.name+"="+this.value+";expires="+expires;
	document.cookie = this.prefix+this.name+"_expire"+"=\""+expires+"\";expires="+expires;
	return this;
};

/**
 * Method to delete the cookie
 *
 * @params [name, [prefix]]
 */
Cookie.prototype.del = function() {
    // Overwriting class attributes setted by Constructor. 
	switch(arguments.length) {
    case 2: this.prefix = arguments[1];
	case 1: this.name = arguments[0];
	}
	document.cookie = this.prefix+this.name+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	document.cookie = this.prefix+this.name+"_expire"+"=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	return this;
};

/**
 * Method to update the cookie value and expiration time
 *
 * @params  [name[,values[,days[,prefix]]]] OR
 *          [value,[days]]
 */
Cookie.prototype.update = function() {
    // Overwriting class attributes setted by Constructor.
	switch(arguments.length) {
    case 4: this.prefix = arguments[3];
	case 3: this.name = arguments[0];
			this.value = arguments[1];
			this.days = arguments[2];
			break;
	case 2: this.days = arguments[1];
	case 1: this.value = arguments[0];
	}
	this.del(); // Remove cookie
	this.create(); // Recreate the cookie with news attributes values
	return this;
};

/**
 * Method to get a cookie value and create 
 * an instance of the Cookie Class
 * 
 * @params [name[,prefix]]
 */
Cookie.prototype.get = function() {
	switch(arguments.length) {
    case 2: this.prefix = arguments[1];
	case 1: this.name = arguments[0];
	}
	this.value = this.val();
	var date = this.searchCookie(this.prefix+this.name+"_expire"),
		today = new Date(),
		expires = new Date(date),millisecondsPerDay = 1000 * 60 * 60 * 24,
		millisBetween = expires.getTime() - today.getTime();
	this.days = (millisBetween / millisecondsPerDay).toFixed(0);
	return this;
};

/**
 * Method to get the Cookie value
 *
 * @params [name[,prefix]]
 */
Cookie.prototype.val = function() {
	switch(arguments.length) {
    case 2: this.prefix = arguments[1];
	case 1: this.name = arguments[0];
	}
	this.value = this.searchCookie(this.prefix+this.name);
	return this.value;
};