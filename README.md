# cookie.class.js

This class will help you to work with browser's cookies.

##DONE:

[2014-02-24] 
  
  * Use RegEx patterns to get cookie value;
  * Class documentation
  * Cookie prefix laod by argument

# Usage

Call the class in your projects <head>

```html
<script src="cookie.class.js"></script>
```

## Create
```js
var cookie = new Cookie('name','value',1);
cookie.create();
// OR JUST CREATE IT
new Cookie().create('name','value',1);
```

## Select
```js
var cookie = new Cookie().get('name');
```

## Update
```js
var cookie = new Cookie().get('name');
cookie.update('New Value');
// OR JUST UPDATE IT
new Cookie().update('name','New Value',3);
```

## Delete
```js
var cookie = new Cookie().get('name');
cookie.del();
// OR JUST UPDATE IT
new Cookie().del('name');
```


# License

MIT-License (see LICENSE file).


# Contributing

Your contribution is welcome.


# TODO

  * Provide a live example
  * Crete a load arguments method
  * Find another way to take the cookie expiration time (In this current version I save an extra cookie to get the expiration value)
